/**
 * @format
 */
import { Navigation } from "react-native-navigation";
import home from './src/screens/Home';
import tab2 from './src/screens/tab2';
import ItemInfo from './src/screens/ItemInfo';
import Video from './src/screens/Video'
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';
import { checkFirebase } from "./src/utils/functions";

Navigation.registerComponent('home', () => gestureHandlerRootHOC(home));
Navigation.registerComponent('itemInfo', () => gestureHandlerRootHOC(ItemInfo));
Navigation.registerComponent('video', () => gestureHandlerRootHOC(Video));
Navigation.registerComponent('tab2', () => gestureHandlerRootHOC(tab2));

Navigation.events().registerAppLaunchedListener(() => {

    Navigation.setRoot({
        root: {
            bottomTabs: {
                id: 'BOTTOM_TABS_LAYOUT',
                children: [
                    {
                        stack: {
                            id: '0',
                            children: [
                                {
                                    component: {
                                        id: 'home',
                                        name: 'home'
                                    }
                                }

                            ],
                            options: {
                                topBar: {
                                    visible: false,
                                },
                                bottomTab: {
                                    icon: require('./home.png')
                                }
                            }
                        }
                    },
                    {
                        stack: {
                            id: '1',
                            children: [
                                {
                                    component: {
                                        id: 'tab2',
                                        name: 'tab2'
                                    }
                                }

                            ],
                            options: {
                                topBar: {
                                    visible: false,
                                },
                                bottomTab: {
                                    icon: require('./home.png')
                                }
                            }
                        }
                    }

                ]
            }

        }
    });
});

checkFirebase()
