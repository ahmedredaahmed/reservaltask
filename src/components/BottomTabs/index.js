/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback, TouchableOpacity
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import styles from './styles'
import { Navigation } from 'react-native-navigation'
const BottomTabs = (props) => {
    const tabs = [{ name: 'home', size: 24, id: 0 },
    { name: 'heart', size: 24, id: 1 },
    { name: 'shopping-cart', size: 24, id: 1 },
    { name: 'user', size: 24, id: 1 },
    ]
    return (<View style={styles.bottomTabs}>
        {tabs.map((item, key) => <TouchableOpacity style={styles.tab} onPress={() => {
            Navigation.mergeOptions(props.componentId, {
                bottomTabs: {
                    currentTabId: item.id
                }
            });
        }} key={key}>
            <View style={styles.tab}>
                <Feather {...item} color="#B4B4B4" />
                <Text style={styles.tabText}>{item.name}</Text></View>
        </TouchableOpacity>
        )}
    </View>)
}

export default BottomTabs;
