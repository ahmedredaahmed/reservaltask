import {
    StyleSheet,
    Dimensions
} from 'react-native';


export default StyleSheet.create({

    bottomTabs: {
        height: 55,
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    tab: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    tabText: {
        fontSize: 12,
        color: '#B4B4B4'
    },
});