/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { forwardRef } from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import styles from './styles'
const FilterButton = (props) => {

    return (<TouchableWithoutFeedback onPress={() => props.showBottomSheet()}  >
        <View style={styles.FilterButton} >
            <Feather name="search" size={20} color="#B4B4B4" />
            <Text style={styles.FilterButtonText}>Search recipes...</Text>
        </View>
    </TouchableWithoutFeedback>);
}

export default FilterButton;
