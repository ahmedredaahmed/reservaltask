import {
    StyleSheet,
    Dimensions
} from 'react-native';


export default StyleSheet.create({

    FilterButton: {
        alignItems: 'center',
        paddingStart: '7%',
        flexDirection: 'row',
        height: '6.5%',
        width: '85%',
        backgroundColor: '#fff',
        borderRadius: 1000,
        marginVertical: '8%'
    },
    FilterButtonText: {
        fontSize: 15,
        paddingStart: '5%',
        color: '#B4B4B4'
    },
});