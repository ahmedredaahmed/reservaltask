/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    FlatList
} from 'react-native';
import styles from './styles'
const HorizontalCategoryList = (props) => {
    const data = ['cook New', 'Breakfast', 'Low Price', 'cook New', 'Breakfast', 'Low Price', 'cook New', 'Breakfast', 'Low Price']
    return (<View style={styles.HorizontalCategoryListContainer}>
        <FlatList
            data={data}
            horizontal={true}
            contentContainerStyle={{ paddingEnd: 50, paddingStart: 15 }}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item, index }) => {
                return (<TouchableWithoutFeedback onPress={() => alert('rree')}>
                    <View style={styles.HorizontalCategoryListListItem}>
                        <Text style={styles.HorizontalCategoryListItemText}>
                            {item}
                        </Text>
                    </View></TouchableWithoutFeedback>)
            }}
        />
    </View>)
};

export default HorizontalCategoryList;
