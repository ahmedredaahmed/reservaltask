import {
    StyleSheet,
    Dimensions
} from 'react-native';


export default StyleSheet.create({
    HorizontalCategoryListContainer: {
        height: '15%',
        width: '100%',
    }, HorizontalCategoryListListItem: {

        paddingVertical: 11, paddingHorizontal: 15, backgroundColor: '#8C8C8C', marginStart: 10,
        borderRadius: 12,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    }, HorizontalCategoryListItemText: {
        fontSize: 14,
        color: '#f2f2f2'
    },
});