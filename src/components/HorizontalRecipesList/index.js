/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    FlatList,

    ImageBackground,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import styles from './styles'
import foodImage from '../../assets/images/foodImage.jpg'
import { Navigation } from 'react-native-navigation';
const HorizontalRecipesList = (props) => {
    const data = [{ des: 'Beef \nBurger' }, { des: 'Beef \nBurger' }, { des: 'Beef \nBurger' }, { des: 'Beef \nBurger' }, { des: 'Beef \nBurger' }, { des: 'Beef \nBurger' },]

    return (<View style={styles.HorizontalRecipesListContainer}>
        <FlatList
            data={data}
            horizontal={true}
            contentContainerStyle={{ paddingEnd: 50, }}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item, index }) => {
                return (<TouchableOpacity onPress={() => {
                    Navigation.push(props.componentId, {
                        component: {
                            name: 'itemInfo'
                        }
                    })
                }}>
                    <ImageBackground source={foodImage}
                        imageStyle={{ borderRadius: 13, }}
                        style={styles.HorizontalRecipesListItem} >
                        <View style={{ flexDirection: 'row', padding: '8%', justifyContent: 'space-between', alignSelf: 'stretch' }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Feather name='heart' size={20.5} color="#fff" />
                                <Text style={styles.HorizontalRecipesListItemText3}>
                                    54
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Feather name='smartphone' size={20.5} color="#fff" />
                                <Text style={styles.HorizontalRecipesListItemText1}>
                                    <Text style={styles.HorizontalRecipesListItemText2}>6</Text>/9
                                </Text>
                            </View>
                        </View>
                        <View style={{ flex: 1, padding: '8%', justifyContent: 'flex-end' }}>
                            <Text style={styles.HorizontalRecipesListItemText4}>
                                {item.des}
                            </Text>
                        </View>
                    </ImageBackground></TouchableOpacity>)
            }}
        />
    </View>)
};

export default HorizontalRecipesList;
