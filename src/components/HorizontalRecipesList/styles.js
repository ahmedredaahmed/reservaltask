import {
    StyleSheet,
    Dimensions
} from 'react-native';


export default StyleSheet.create({

    HorizontalRecipesListContainer: {
        flex: 1,
        width: '100%',
    },
    HorizontalRecipesListItem: {
        flex: 1,
        width: Dimensions.get('window').width * 0.73,
        backgroundColor: '#8C8C8C', marginStart: 25,
        borderRadius: 13,
        alignSelf: 'stretch',
    }, HorizontalRecipesListItemText: {
        fontSize: 14,
        color: '#f2f2f2'
    },
    HorizontalRecipesListItemText1: {
        paddingStart: 8,
        fontSize: 18,
        color: '#f2f2f2'
    }, HorizontalRecipesListItemText2: {
        fontSize: 22,
        color: '#f2f2f2',
        fontWeight: 'bold'
    }, HorizontalRecipesListItemText3: {
        paddingStart: 8,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#f2f2f2'
    },
    HorizontalRecipesListItemText4: {
        fontSize: 32,
        fontWeight: 'bold',
        color: '#f2f2f2'
    },
});