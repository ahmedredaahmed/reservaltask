import React, { useCallback, useState } from 'react';
import { View, Text } from 'react-native';
import Slider from 'rn-range-slider';

import Thumb from './components/Thumb';
import Rail from './components/Rail';
import RailSelected from './components/RailSelected';
import Notch from './components/Notch';
import Label from './components/Label';

import styles from './styles';

const SliderScreen = () => {


  const [low, setLow] = useState(0);
  const [high, setHigh] = useState(100);
  const [min, setMin] = useState(0);
  const [max, setMax] = useState(100);
  const [floatingLabel, setFloatingLabel] = useState(true);

  const renderThumb = useCallback(() => <Thumb />, []);
  const renderRail = useCallback(() => <Rail />, []);
  const renderRailSelected = useCallback(() => <RailSelected />, []);
  const renderLabel = useCallback(value => <Label text={value} />, []);
  const renderNotch = useCallback(() => <Notch />, []);
  const handleValueChange = useCallback((low, high) => {
    setLow(low);
    setHigh(high);
  }, []);
  return <>
    <View style={[styles.horizontalContainer, { marginBottom: 15, }]}>
      <Text style={{ fontSize: 15, color: '#000' }}>Cooking Time</Text>
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.valueText2}>{low}</Text>
        <Text style={[styles.valueText, { alignSelf: 'center', marginHorizontal: 10 }]}>to</Text>
        <Text style={styles.valueText2}>{high}</Text>
      </View>
    </View>
    <Slider
      style={styles.slider}
      min={min}
      max={max}
      step={1}
      floatingLabel={floatingLabel}
      renderThumb={renderThumb}
      renderRail={renderRail}
      renderRailSelected={renderRailSelected}
      renderLabel={renderLabel}
      renderNotch={renderNotch}
      onValueChanged={handleValueChange}
    />
    <View style={styles.horizontalContainer}>
      <Text style={styles.valueText}>{min}kg</Text>
      <Text style={styles.valueText}>{max}kg</Text>
    </View>

  </>;
};

export default SliderScreen;
