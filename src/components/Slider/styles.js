import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  root: {
    alignItems: 'stretch',
    padding: 12,
    flex: 1,
    backgroundColor: '#555',
  },
  slider: {
  },
  button: {
  },
  header: {
    alignItems: 'center',
    backgroundColor: 'black',
    padding: 12,
  },
  horizontalContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',

  },
  text: {
    color: '#BDBDBD',
    fontSize: 20,
  },
  valueText: {

    color: '#ADADAD',
    fontSize: 10,
  }, valueText2: {

    color: '#ADADAD',
    fontSize: 20, borderRadius: 10,
    paddingVertical: 3, paddingHorizontal: 13, backgroundColor: '#F3F3F3'
  },
});
