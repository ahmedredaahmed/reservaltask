/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {

    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import Slider from '../../components/Slider'
import styles from './styles'
const ItemSelected = (props) => {
    return (<View style={[styles.itemContainer, styles.shadow]}>
        <Text style={styles.text}>{props.value}</Text>
    </View>)
}

const Item = (props) => {
    return (<View style={[styles.itemSelectedContainer, styles.shadow]}>
        <Text style={styles.textSelected}>{props.value}</Text>
    </View>)
}
const Groupe = (props) => {
    let selectedValue = props.value

    return (<React.Fragment>
        <View style={styles.row}>
            <Text style={styles.textSelected}>{props.mainText}</Text>
            <Text style={{ color: '#ACACAC' }}>{props.secText}</Text>
        </View>
        <View style={[styles.row, { flexWrap: 'wrap', justifyContent: 'flex-start', }]}>
            {props.values.map((item, key) => (
                <TouchableOpacity onPress={() => props.setValue(item.value)}>
                    {item.value == selectedValue ? <ItemSelected key={key} value={item.label} /> : <Item key={key} value={item.label} />}
                </TouchableOpacity>))}
        </View>
    </React.Fragment>)
}
const FilterContent = (props) => {
    const data1 = { mainText: 'Cooking Time', secText: 'min', values: [{ label: 10, value: 10 }, { label: 20, value: 20 }, { label: 30, value: 30 }, { label: 40, value: 40 },], initValue: 10 }
    const data2 = { mainText: 'Daily regimen', secText: '', values: [{ label: 'breakfast', value: 10 }, { label: 'lunch', value: 20 }, { label: 'Dinner', value: 30 }, { label: 'Snack', value: 40 },], initValue: 10 }
    const data3 = { mainText: 'Cuisine', secText: '', values: [{ label: 'American', value: 10 }, { label: 'Arabic', value: 20 }, { label: 'Asian', value: 30 }, { label: 'Chinese', value: 40 },], initValue: 10 }

    const [value1, setValue1] = useState(data1.initValue)
    const [value2, setValue2] = useState(data2.initValue)
    const [value3, setValue3] = useState(data3.initValue)


    const reset = () => {
        setValue1(data1.initValue)
        setValue2(data2.initValue)
        setValue3(data3.initValue)
    }
    const save = () => {
        alert(`value1 => ${value1}\nvalue2 => ${value2}\nvalue3 => ${value3}`)
        props.hideBottomSheet()
    }

    return (<View
        style={styles.container}
    >
        <View style={styles.headContainer}>
            <View style={{ flex: 1 }}>
                <Text style={styles.text2}>Filters</Text>
            </View>
            <TouchableOpacity onPress={reset}>
                <Text style={styles.text3}>Reset</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={save}>
                <Text style={styles.text3}>Go!</Text>
            </TouchableOpacity>
        </View>
        <View style={styles.contentContainer}>
            <Groupe {...data1} value={value1} setValue={setValue1} />
            <Groupe {...data2} value={value2} setValue={setValue2} />
            <View style={{ alignSelf: 'stretch', }}>
                <Slider />
            </View>
            <Groupe {...data3} value={value3} setValue={setValue3} />
        </View>
    </View >);
};


export default FilterContent;
