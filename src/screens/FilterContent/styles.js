import {
    StyleSheet,
    Dimensions
} from 'react-native';


export default StyleSheet.create({
    text: { fontSize: 15, color: '#fff' },
    itemContainer: { marginHorizontal: 2, marginVertical: 5, backgroundColor: '#8B8B8B', borderRadius: 15, paddingHorizontal: 10, minWidth: 60, justifyContent: 'center', alignItems: 'center', paddingVertical: 8 },
    textSelected: { fontSize: 15, color: '#000' },
    itemSelectedContainer: { marginHorizontal: 2, marginVertical: 5, backgroundColor: '#fff', borderRadius: 15, paddingHorizontal: 10, minWidth: 60, justifyContent: 'center', alignItems: 'center', paddingVertical: 8, },

    row: { flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'space-between' },
    container: {
        backgroundColor: '#fff',
        flex: 1,
        alignSelf: 'stretch',
        borderTopRightRadius: 30, borderTopLeftRadius: 30
    },
    contentContainer: { flex: 1, alignSelf: 'stretch', padding: '8%', justifyContent: 'space-around', },
    headContainer: { paddingHorizontal: '8%', flexDirection: 'row', backgroundColor: '#F2F2F2', alignItems: 'center', height: '10%', borderTopRightRadius: 30, borderTopLeftRadius: 30 },
    text2: { fontSize: 23, fontWeight: 'bold', color: '#717171' },
    text3: { fontSize: 18, fontWeight: 'bold', color: '#B5B5B5' },
    text3: { marginStart: '5%', fontSize: 18, fontWeight: 'bold', color: '#B5B5B5' },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }
});