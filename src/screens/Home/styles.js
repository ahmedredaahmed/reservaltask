import {
    StyleSheet,
    Dimensions
} from 'react-native';


export default StyleSheet.create({
    View: {
        flex: 1, alignSelf: 'stretch', backgroundColor: '#f2f2f2', alignItems: 'center',
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    }
});