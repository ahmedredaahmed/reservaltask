/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    FlatList,
    Dimensions,
    ImageBackground,
    TouchableWithoutFeedback
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import foodImage from '../../assets/images/foodImage2.jpg'
import { Navigation } from "react-native-navigation";
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Swiper from 'react-native-swiper'
import styles from './styles'
import Video from '../Video';
import { share } from '../../utils/functions'
const Counter = (props) => {
    const [count, setCount] = useState(10)
    return (<View style={{ flexDirection: 'row', marginTop: 5 }}>
        <TouchableWithoutFeedback onPress={() => {
            if (count > 1)
                setCount(count - 1)
        }}>
            <AntDesign name='minussquare' size={24} color="#B2B2B2" />
        </TouchableWithoutFeedback>
        <Text style={{ marginHorizontal: '12%', color: '#B2B2B2' }}>{count}</Text>
        <TouchableWithoutFeedback onPress={() => setCount(count + 1)}>
            <AntDesign name='plussquare' size={24} color="#B2B2B2" />
        </TouchableWithoutFeedback>
    </View>)
}
const ItemInfo = (props) => {
    Navigation.mergeOptions(props.componentId, {

        bottomTabs: {
            visible: false,

        },
    });

    const Header = () => {
        return (<Swiper style={styles.swiper} showsPagination={true}
            renderPagination={(index, total, context) => {
                return (
                    <View
                        style={styles.pagination} >
                        <View style={styles.header}>
                            <TouchableOpacity onPress={() => { Navigation.pop(props.componentId) }} >
                                <Feather name='chevron-left' size={30} color="#fff" />
                            </TouchableOpacity>
                            <View style={styles.row}>
                                <TouchableWithoutFeedback>
                                    <View style={styles.row}>
                                        <Feather name='heart' size={21} color="#fff" />
                                        <Text style={styles.HorizontalRecipesListItemText3}>
                                            54
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback onPress={() => share({ title: 'hello', url: 'google.com' })} >
                                    <View style={{ marginHorizontal: 28 }}>
                                        <Feather name='share-2' size={21} color="#fff" />
                                    </View>
                                </TouchableWithoutFeedback>
                                <TouchableWithoutFeedback>
                                    <Feather name='eye-off' size={21} color="#fff" />
                                </TouchableWithoutFeedback>
                            </View>
                        </View>
                        <View style={styles.nameContainer}>
                            <View style={styles.name}>
                                <Text style={styles.HorizontalRecipesListItemText4}>
                                    {'Beef \nBurger'}
                                </Text>
                                <View style={styles.count}>
                                    <Feather name='smartphone' size={20.5} color="#fff" />
                                    <Text style={styles.HorizontalRecipesListItemText1}>
                                        <Text style={styles.HorizontalRecipesListItemText2}>{index + 1}</Text>/{total}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>)
            }}
        >
            <ImageBackground source={foodImage}
                imageStyle={{ height: "100%" }} style={styles.image}>

            </ImageBackground>
            <ImageBackground source={foodImage}
                imageStyle={{ height: "100%" }} style={styles.image}>

            </ImageBackground>
        </Swiper>)

    }
    const HorizontalCategoryList = () => {
        const data = ['Beff', 'Tomatoes', 'Onions', 'lettuce', 'Cares bread', 'ketchup', 'Mayonnaise']
        return (<View style={styles.HorizontalCategoryListContainer}>
            <FlatList
                data={data}
                horizontal={true}
                contentContainerStyle={{ paddingEnd: 50, paddingStart: 15 }}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item, index }) => {
                    return (
                        <View style={styles.HorizontalCategoryListListItem}>
                            <Text style={styles.HorizontalCategoryListItemText}>
                                {item}
                            </Text>
                        </View>)
                }}
            />
        </View>)
    }
    const Nutrition = () => {
        let date = [{ label: 'CAL', val: '587', fill: 58 }, { label: 'FAT', val: '7g', fill: 70 }, { label: 'CARES', val: '80g', fill: 80 }, { label: 'PROT', val: '49g', fill: 49 }]
        return (<View style={styles.Nutrition}>
            <Text style={styles.NutritionText}>
                Nutrition Information per Serving
            </Text>
            <View style={styles.CircularProgressContainer}>
                {date.map((item, key) => <AnimatedCircularProgress
                    key={key}
                    size={Dimensions.get('screen').width * 0.2}
                    width={3}
                    fill={item.fill}
                    tintColor="#9A9A9A"
                    backgroundColor="#fff"
                >{(fill) => (<View style={styles.CircularProgressView}>
                    <Text style={styles.CircularProgressText}>{item.label}</Text>
                    <Text style={styles.CircularProgressText2}>{item.val}</Text>
                </View>
                )}
                </AnimatedCircularProgress>)}

            </View>
        </View>)
    }

    const Action = () => {
        return (<View style={styles.action}>
            <Feather name='shopping-cart' size={20.5} color="#fff" />
            <Text style={[styles.HorizontalRecipesListItemText2, { marginStart: 10, fontSize: 18 }]}>Add Missing Ingredients</Text>
        </View>)
    }
    const Servings = () => {
        return (<View style={styles.ServingsContainer}>
            <View>
                <Text style={styles.Servings}>
                    Servings
                </Text>
                <Counter />
            </View>
            <View style={{}}>
                <Text style={styles.textName}>
                    Cooking time
                </Text>
                <Text style={styles.time}>
                    1 hr 17 min
                </Text>
            </View>
        </View>

        )
    }
    // const Video = () => {
    //     return (<TouchableOpacity style={styles.videoAction} onPress={() => {
    //         Navigation.push(props.componentId, {
    //             component: {
    //                 name: 'video'
    //             }
    //         })
    //     }}><Text style={[styles.videoActionText]}>Start Cooking video</Text>
    //     </TouchableOpacity>)
    // }
    return (<SafeAreaView style={styles.View}>
        <ScrollView>
            <View style={styles.View}>
                <Header />
                <HorizontalCategoryList />
                <Servings />

                {/* <Video /> */}
                <Nutrition />
            </View>
            <View style={{ marginTop: 10 }}>
                <Video />
            </View>
        </ScrollView>
        <Action />
    </SafeAreaView>);
};



export default ItemInfo;
