import {
    StyleSheet,
    Dimensions
} from 'react-native';


export default StyleSheet.create({
    View: {
        flex: 1, alignSelf: 'stretch', backgroundColor: '#f2f2f2', alignItems: 'center',
    },
    FilterButton: {
        alignItems: 'center',
        paddingStart: '7%',
        flexDirection: 'row',
        height: '6.5%',
        width: '85%',
        backgroundColor: '#fff',
        borderRadius: 1000,
        marginVertical: '8%'
    },
    FilterButtonText: {
        fontSize: 15,
        paddingStart: '5%',
        color: '#B4B4B4'
    },
    HeaderContainer: {
        flex: 1,
        width: '100%',
    },
    HorizontalCategoryListContainer: {
        marginTop: 20,
        width: '100%',
    },
    bottomTabs: {
        height: 55,
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    tab: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    tabText: {
        fontSize: 12,
        color: '#B4B4B4'
    },
    HorizontalCategoryListItemText: {
        fontSize: 13,
        color: '#f2f2f2'
    },
    HorizontalRecipesListItemText: {
        fontSize: 14,
        color: '#f2f2f2'
    },
    HorizontalRecipesListItemText1: {
        paddingStart: 8,
        fontSize: 18,
        color: '#f2f2f2'
    }, HorizontalRecipesListItemText2: {
        fontSize: 20,
        color: '#f2f2f2',
        fontWeight: 'bold'
    }, HorizontalRecipesListItemText3: {
        paddingStart: 8,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#f2f2f2'
    },
    HorizontalRecipesListItemText4: {
        fontSize: 32,
        fontWeight: 'bold',
        color: '#f2f2f2'
    },
    HorizontalRecipesListItem: {

        height: '100%',
        alignSelf: 'stretch',
    },
    HorizontalCategoryListListItem: {

        paddingVertical: 3, paddingHorizontal: 10, backgroundColor: '#8C8C8C', marginStart: 10,
        borderRadius: 5,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    }
    , swiper: { height: Dimensions.get('screen').height * 0.35, alignSelf: 'stretch', },
    pagination: {
        position: 'absolute', height: '100%',
        width: '100%'
    },
    header: { flexDirection: 'row', padding: '5%', justifyContent: 'space-between', alignSelf: 'stretch' },
    row: { flexDirection: 'row', alignItems: 'center' },
    count: { flexDirection: 'row', alignSelf: 'flex-end', alignItems: 'center' },
    name: { flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'stretch' },
    nameContainer: { flex: 1, padding: '8%', justifyContent: 'flex-end' },
    image: { flex: 1, height: "100%", alignSelf: 'stretch', },
    action: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: 50, borderRadius: 1000, marginHorizontal: '8%', marginVertical: 10, backgroundColor: '#8C8C8C', alignSelf: 'stretch' },
    Nutrition: { alignSelf: 'stretch', paddingVertical: '2%', paddingStart: '8%', paddingEnd: '4%', },
    CircularProgressView: { alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff', flex: 1, alignSelf: 'stretch' },
    CircularProgressText: { color: '#999999', marginBottom: -5 },
    CircularProgressText2: { color: '#999999', fontSize: 20 },
    NutritionText: { fontSize: 18, fontWeight: 'bold', color: '#0F0F0F' },
    CircularProgressContainer: { marginTop: '5%', flexDirection: 'row', justifyContent: 'space-around' },
    videoAction: { alignItems: 'center', justifyContent: 'center', height: 50, marginHorizontal: '8%', marginBottom: 10, backgroundColor: '#fff', alignSelf: 'stretch' },
    videoActionText: { fontSize: 15, marginStart: 10, color: '#B2B2B2' },
    Servings: { fontSize: 18, fontWeight: 'bold', color: '#0F0F0F' },
    ServingsContainer: { flexDirection: 'row', alignSelf: 'stretch', paddingHorizontal: '8%', paddingVertical: '6%' },
    textName: { fontSize: 18, fontWeight: 'bold', color: '#0F0F0F' },
    time: { fontSize: 12, fontWeight: 'bold', marginTop: 7, color: '#B2B2B2' }
});