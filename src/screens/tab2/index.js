/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    FlatList,
    Dimensions,
    ImageBackground,
    TouchableWithoutFeedback
} from 'react-native';
import { Navigation } from "react-native-navigation";
import BottomSheet from '../../../BottomSheet';
import FilterButton from '../../components/FilterButton'
import HorizontalRecipesList from '../../components/HorizontalRecipesList'
import HorizontalCategoryList from '../../components/HorizontalCategoryList'
import BottomTabs from '../../components/BottomTabs'
import FilterContent from '../FilterContent'
import styles from './styles'
const Home = (props) => {
    let thisBottomSheet
    Navigation.mergeOptions(props.componentId, {

        bottomTabs: {
            visible: false,

        },
    });
    return (<React.Fragment>
        <SafeAreaView style={styles.View}>
            <View style={styles.View}>
                <Text style={{ fontSize: 20 }}>
                    Coming Soon
                </Text>
            </View>
            <BottomTabs {...props} />
        </SafeAreaView>

    </React.Fragment>);
};


export default Home;
