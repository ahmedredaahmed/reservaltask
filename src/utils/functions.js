import Share from 'react-native-share'
import { Platform } from 'react-native'

import firebase from "react-native-firebase";
export async function share({ title, url }) {
  if (!url) return
  // very important below: \u00A0
  const subject = `${title}\u00A0 `
  const options = Platform.select({
    ios: {
      activityItemSources: [
        {
          placeholderItem: { type: 'url', content: url },
          item: {
            copyToPasteBoard: {
              type: 'url',
              content: url
            },
            default: {
              type: 'text',
              content: `${subject}\n${url}`
            },
            mail: {
              type: 'text',
              content: `${subject}\n${url}`
            },
          },
          subject: {
            default: subject,
          },
        },
      ],
    },
    default: {
      title,
      subject,
      message: subject,
      url,
    },
  })
  await Share.open(options)
    .then(({ app }) => {
      console.log('app', app) // this tells you what platform was chosen
      return true
    })
    .catch((err) => {
      if (err) console.log(err)
      return true
    })
}

export async function checkFirebase() {
  try {
    await firebase.messaging().requestPermission();
    const fcmToken = await firebase.messaging().getToken();
    alert(`your fcmToken from firebase =>>\n${fcmToken}`)
    firebase
      .notifications()
      .getInitialNotification()
      .then(async (notificationOpen) => {
        console.log("🚀 ~ file: functions.js ~ line 61 ~ .then ~ notificationOpen", notificationOpen)


      })
    firebase.messaging().onTokenRefresh(fcmToken => {
      // Process your token as required
      alert(`your new fcmToken =>>${fcmToken}`)
    });
  } catch (error) {
    console.log("🚀 ~ file: index.js ~ line 82 ~ error", error)

  }
}